<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'teach'], function () {
	Route::match(['get','post'],'login', 'LecturerLoginController@login');
	Route::match(['get', 'post'],'register','RegisterController@register');
	Route::get('register/success', 'RegisterController@success');
	Route::get('dashboard',function(){
		return view('dashboard/index');
	});
	Route::group(['prefix' => 'course'], function () {
		Route::get('create', 'CourseController@create');
	});
	Route::get('test','LoginController@test');
});
