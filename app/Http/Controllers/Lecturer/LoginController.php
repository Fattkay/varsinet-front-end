<?php
namespace App\Http\Lecturer\Controllers;

use App\Http\Controllers;
use App\Helpers\Contracts\MakeRequestContract;
use Illuminate\Http\Request;;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller {

    public function login(Request $request, MakeRequestContract $requestContract){
        $data = [];
        if($request->method() == 'POST'){
            $data = $this->validateInput($request);
            $response = $requestContract->post('/student/login',$data);
            if($response->statusCode == 200){
                var_dump($response->response);
                return redirect('/dashboard');
            } else if($response->statusCode == 400) {
                $data['error'] = 'Wrong email or password';
                var_dump($response->error_response);
            }
        }
        return view('login/index', $data);
    }

    public function validateInput(Request $request){
        $validator = Validator::make($request->all(),[
            'email'     => 'required',
            'password'  => 'required',
        ]);
        if($validator->fails()){
            return redirect('/login')
                ->withInput()
                ->withErrors($validator);
        } else {
            return [
                'email' => $request->input('email'),
                'password' => $request->input('password')
            ];
        }
    }

    public function test(MakeRequestContract $make){
        $boom = $make->test();
        return view('dashboard.index',compact($boom));
    }
}