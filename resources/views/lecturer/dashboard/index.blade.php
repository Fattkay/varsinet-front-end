@extends('master')
@section('content')
 <section id="main-content">
          <section class="wrapper">            
                    <div class="row state-overview">
                  <div class="col-lg-4">
      
                  <section class="panel">
                      <div class="profile-widget profile-widget-info">
                          <div class="panel-body">
                            <div class="col-lg-4 col-sm-4 profile-widget-name">
                              <h4>Fatt Kay</h4>               
                              <div class="follow-ava">
                                  <img src="img/avatar1_small.png" alt="">
                              </div>
                              <h6>Teacher</h6>
                            </div>
                            <div class="col-lg-8 col-sm-8 follow-info">
                                <p>Welcome Fattkay, Please check your uploaded courses or upload new ones.</p>
                                <p>@fattkay</p>
                                
                            </div>
                            
                          
                      </div>
                  </section>
      
                </div>
      
                <div class="state col-lg-8">
                    <div class="row">
                      <div class="col-lg-3 col-sm-6">
                          <section class="panel">
                              <div class="symbol">
                                  <i class="icon_tags_alt"></i>
                              </div>
                              <div class="value">
                                  <h1>2</h1>
                                  <p>Courses</p>
                              </div>
                          </section>
                      </div>
                      <div class="col-lg-3 col-sm-6">
                          <section class="panel">
                              <div class="symbol">
                                  <i class="icon_globe"></i>
                              </div>
                              <div class="value">
                                  <h1>14</h1>
                                  <p>Students</p>
                              </div>
                          </section>
                      </div>
                      
                      <div class="col-lg-3 col-sm-6">
                          <section class="panel">
                              <div class="symbol">
                                  <i class="icon_currency"></i>
                              </div>
                              <div class="value">
                                  <h1>#5,500</h1>
                                  <p>Total Profit</p>
                              </div>
                          </section>
                      </div>
                    </div>

                    
                </div>    
               
              </div>
              
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
						  
						Submitted Courses         
                 </header>
                          <table class="table table-striped border-top" id="sample_1">
  <thead>
    <tr>
                                   
      <th>
        Select
      </th>
      <th>
	  Course Name
        
      </th>
      <th>
	  Date Uploaded
        
      </th>
      <th> 
	  Teacher
	    
      </th>
      <th>
	  No. Of Students      
        
      </th>
	  <th> 
	  Status
	    
      </th>
    </tr>
  </thead>
  <tbody>  
  
    <tr class="odd gradeX">
                              <td><input type="checkbox" class="checkboxes" value="1" /></td>
      <td>
        
Introduction To Matrix
      </td>
      <td>
        2016-01-17
      </td>
      <td>
        Mr Sangotola
      </td>
      <td>
        14
      </td>
      <td>
       Active
      </td>
    </tr>
	
  
    <tr class="odd gradeX">
                              <td><input type="checkbox" class="checkboxes" value="1" /></td>
      <td>
        
Introduction To Matrix
      </td>
      <td>
        2016-01-17
      </td>
      <td>
        Mr Sangotola
      </td>
      <td>
        14
      </td>
      <td>
       Active
      </td>
    </tr>
	
    <tr class="odd gradeX">
                              <td><input type="checkbox" class="checkboxes" value="1" /></td>
      <td>
        
Introduction To Matrix
      </td>
      <td>
        2016-01-17
      </td>
      <td>
        Mr Sangotola
      </td>
      <td>
        14
      </td>
      <td>
       Active
      </td>
    </tr>
	
    <tr class="odd gradeX">
                              <td><input type="checkbox" class="checkboxes" value="1" /></td>
      <td>
        
Introduction To Matrix
      </td>
      <td>
        2016-01-17
      </td>
      <td>
        Mr Sangotola
      </td>
      <td>
        14
      </td>
      <td>
       Pending
      </td>
    </tr>
	
    <tr class="odd gradeX">
                              <td><input type="checkbox" class="checkboxes" value="1" /></td>
      <td>
        
Introduction To Matrix
      </td>
      <td>
        2016-01-17
      </td>
      <td>
        Mr Sangotola
      </td>
      <td>
        14
      </td>
      <td>
       Active
      </td>
    </tr>
	
    <tr class="odd gradeX">
                              <td><input type="checkbox" class="checkboxes" value="1" /></td>
      <td>
        
Introduction To Matrix
      </td>
      <td>
        2016-01-17
      </td>
      <td>
        Mr Sangotola
      </td>
      <td>
        14
      </td>
      <td>
       Pending
      </td>
    </tr>
	  
  </tbody>
</table>
                      </section>
                  </div>
              </div>

				  
				 
              

          </section>
      </section>
      
  </section>
@endsection