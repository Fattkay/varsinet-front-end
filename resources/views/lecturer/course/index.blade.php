@extends('master')
@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            ADD NEW COURSE
                        </header>
                        <div class="panel-body">
                            <form class="form-horizontal " method="get">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Title of course</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="Course Title">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Course Excerpt</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="Course Excerpt">
                                        <span class="help-block">Just some summary of what the whole course is all about.</span>
                                    </div>
                                </div>


                                <!--<div class="form-group">
                                    <label class="col-sm-2 control-label">Disabled</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" id="disabledInput" type="text" placeholder="Disabled ..." disabled>
                                    </div>
                                </div>-->

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Course Access Password</label>

                                    <div class="col-sm-10">
                                        <input type="password"  class="form-control" placeholder="">
                                        <span class="help-block">For secured courses only..</span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Course Category</label>

                                    <div class="col-sm-10">
                                        <select class="form-control input-sm m-bot15">
                                            <option>Diploma</option>
                                            <option>Undergraduate</option>
                                            <option>Postgraduate</option>
                                            <option>Company Training</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">


                                    <label class="col-sm-2 control-label">Course Video URL</label>

                                    <div class="col-sm-10">
                                        <input type="password"  class="form-control" placeholder="URL">
                                        <span class="help-block">Input video URL e.g Youtube</span>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-sm-2 control-label"> Course Tags</label>


                                    <div class="col-sm-10">
                                        <input name="tagsinput" id="tagsinput" class="tagsinput" value="Mathematics,Whogohost, education" />
                                    </div>

                                </div>

                                <div class="col-lg-12">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            Course Details
                                        </header>
                                        <div class="panel-body">
                                            <div class="form">
                                                <form action="#" class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-2">Full Details</label>
                                                        <div class="col-sm-10">
                                                            <textarea class="form-control ckeditor" name="editor1" rows="6"></textarea>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> By submitting this course, you are bound to have agrred to our <a href="#">Terms And Conditions</a>.
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>




                            </form>
                        </div>
                    </section>



                </div>
            </div>



        </section>
    </section>

    </section>
@endsection